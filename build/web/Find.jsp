<%-- 
    Document   : Find
    Created on : Jun 17, 2016, 10:23:26 AM
    Author     : dell
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Find</title>
    </head>
    <body>

        <style>
            html, body {
                height: 100%;
                margin: 0;
                padding: 0;
            }
            #map {
                height: 70%;
                width: 70%;
            }
            #floating-panel {
                position: absolute;
                top: 10px;
                left: 5%;
                z-index: 5;
                background-color: #fff;
                padding: 5px;
                border: 1px solid #999;
                text-align: center;
                font-family: 'Roboto','sans-serif';
                line-height: 30px;
                padding-left: 10px;
            }
        </style>

        <div id="floating-panel">

            <h4>Enter x distance:</h4>
            <input type="text" name="di" id="d"><br>
            <h4>Enter USA Zipcode:</h4>
            <input type="text" name="zipcode" id="address"><br>
            <input type="button" id="submit" value="Geocode">

        </div>
        <div id="map"></div>
        <script>
            function initMap() {
                var map = new google.maps.Map(document.getElementById('map'), {
                    zoom: 12,
                    center: {lat: 38.397, lng: -79.644}
                });
                var geocoder = new google.maps.Geocoder();

                document.getElementById('submit').addEventListener('click', function () {
                    geocodeAddress(geocoder, map);
                });
            }

            function geocodeAddress(geocoder, resultsMap) {
                var di = document.getElementById("d").value;
                var address = document.getElementById('address').value;
                geocoder.geocode({'address': address}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                        var lati = results[0].geometry.location.lat();
                        var lngi = results[0].geometry.location.lng();
                        document.getElementById("l").value = lati;
                        document.getElementById("distance").value = di;
                        document.getElementById("ln").value = lngi;

                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }
        </script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDoEQJGioFYaNU1rmwStjW0qC-JtTelzzQ&callback=initMap">
        </script>
        <br>
        <form action="distance">
            Your Distance:<input type="text" id="distance" name="distance">
            Your Latitude:<input type="text" id="l" name="a">
            Your Longitude:<input type="text" id=ln name="b">
            <input type="submit" value="Find">
        </form>
    </body>
</html>
