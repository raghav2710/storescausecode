package org.apache.jsp;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class Find_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final JspFactory _jspxFactory = JspFactory.getDefaultFactory();

  private static java.util.List<String> _jspx_dependants;

  private org.glassfish.jsp.api.ResourceInjector _jspx_resourceInjector;

  public java.util.List<String> getDependants() {
    return _jspx_dependants;
  }

  public void _jspService(HttpServletRequest request, HttpServletResponse response)
        throws java.io.IOException, ServletException {

    PageContext pageContext = null;
    HttpSession session = null;
    ServletContext application = null;
    ServletConfig config = null;
    JspWriter out = null;
    Object page = this;
    JspWriter _jspx_out = null;
    PageContext _jspx_page_context = null;

    try {
      response.setContentType("text/html;charset=UTF-8");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;
      _jspx_resourceInjector = (org.glassfish.jsp.api.ResourceInjector) application.getAttribute("com.sun.appserv.jsp.resource.injector");

      out.write("\n");
      out.write("\n");
      out.write("\n");
      out.write("<!DOCTYPE html>\n");
      out.write("<html>\n");
      out.write("    <head>\n");
      out.write("        <meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">\n");
      out.write("        <title>Find</title>\n");
      out.write("    </head>\n");
      out.write("    <body>\n");
      out.write("\n");
      out.write("        <style>\n");
      out.write("            html, body {\n");
      out.write("                height: 100%;\n");
      out.write("                margin: 0;\n");
      out.write("                padding: 0;\n");
      out.write("            }\n");
      out.write("            #map {\n");
      out.write("                height: 70%;\n");
      out.write("                width: 70%;\n");
      out.write("            }\n");
      out.write("            #floating-panel {\n");
      out.write("                position: absolute;\n");
      out.write("                top: 10px;\n");
      out.write("                left: 5%;\n");
      out.write("                z-index: 5;\n");
      out.write("                background-color: #fff;\n");
      out.write("                padding: 5px;\n");
      out.write("                border: 1px solid #999;\n");
      out.write("                text-align: center;\n");
      out.write("                font-family: 'Roboto','sans-serif';\n");
      out.write("                line-height: 30px;\n");
      out.write("                padding-left: 10px;\n");
      out.write("            }\n");
      out.write("        </style>\n");
      out.write("\n");
      out.write("        <div id=\"floating-panel\">\n");
      out.write("\n");
      out.write("            <h4>Enter x distance:</h4>\n");
      out.write("            <input type=\"text\" name=\"di\" id=\"d\"><br>\n");
      out.write("            <h4>Enter USA Zipcode:</h4>\n");
      out.write("            <input type=\"text\" name=\"zipcode\" id=\"address\"><br>\n");
      out.write("            <input type=\"button\" id=\"submit\" value=\"Geocode\">\n");
      out.write("\n");
      out.write("        </div>\n");
      out.write("        <div id=\"map\"></div>\n");
      out.write("        <script>\n");
      out.write("            function initMap() {\n");
      out.write("                var map = new google.maps.Map(document.getElementById('map'), {\n");
      out.write("                    zoom: 12,\n");
      out.write("                    center: {lat: 38.397, lng: -79.644}\n");
      out.write("                });\n");
      out.write("                var geocoder = new google.maps.Geocoder();\n");
      out.write("\n");
      out.write("                document.getElementById('submit').addEventListener('click', function () {\n");
      out.write("                    geocodeAddress(geocoder, map);\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("\n");
      out.write("            function geocodeAddress(geocoder, resultsMap) {\n");
      out.write("                var di = document.getElementById(\"d\").value;\n");
      out.write("                var address = document.getElementById('address').value;\n");
      out.write("                geocoder.geocode({'address': address}, function (results, status) {\n");
      out.write("                    if (status === google.maps.GeocoderStatus.OK) {\n");
      out.write("                        resultsMap.setCenter(results[0].geometry.location);\n");
      out.write("                        var marker = new google.maps.Marker({\n");
      out.write("                            map: resultsMap,\n");
      out.write("                            position: results[0].geometry.location\n");
      out.write("                        });\n");
      out.write("                        var lati = results[0].geometry.location.lat();\n");
      out.write("                        var lngi = results[0].geometry.location.lng();\n");
      out.write("                        document.getElementById(\"l\").value = lati;\n");
      out.write("                        document.getElementById(\"distance\").value = di;\n");
      out.write("                        document.getElementById(\"ln\").value = lngi;\n");
      out.write("\n");
      out.write("                    } else {\n");
      out.write("                        alert('Geocode was not successful for the following reason: ' + status);\n");
      out.write("                    }\n");
      out.write("                });\n");
      out.write("            }\n");
      out.write("        </script>\n");
      out.write("        <script async defer\n");
      out.write("                src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyDoEQJGioFYaNU1rmwStjW0qC-JtTelzzQ&callback=initMap\">\n");
      out.write("        </script>\n");
      out.write("        <br>\n");
      out.write("        <form action=\"distance\">\n");
      out.write("            Your Distance:<input type=\"text\" id=\"distance\" name=\"distance\">\n");
      out.write("            Your Latitude:<input type=\"text\" id=\"l\" name=\"a\">\n");
      out.write("            Your Longitude:<input type=\"text\" id=ln name=\"b\">\n");
      out.write("            <input type=\"submit\" value=\"Find\">\n");
      out.write("        </form>\n");
      out.write("    </body>\n");
      out.write("</html>\n");
    } catch (Throwable t) {
      if (!(t instanceof SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          out.clearBuffer();
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
